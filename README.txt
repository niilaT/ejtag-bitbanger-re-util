eJTAG bitbanger - RE util

This project includes a simple JTAG bit banger and a USB-serial-accessible console with a few functions to aid with reverse engineering a (e)JTAG interface.
The JTAG bitbanger code (jtagbitbang.c/.h) itself is supposed to be hardware-independent and easily portable. 
If you want to port it on other architecture you should edit its hardware-dependent part hwdep (currently for Longan Nano).
Also main.c will probably require adjustment for the USB-serial initialization etc. 

The project was made focusing a specific Toshiba MCU so some test details may be specific to it. E.g. a separate EJTAG Enable signal was handled externally.
Check the functions in main.c before using them. 
Default setup was relying on weakish external pull-ups and hence the default speed is low 100kHz.

Notes about jtagbitbang functions: 
When moving to shifting state from Capture you need to first call toShiftXR() function and after that readShiftXRgotoExit1XR() which then gives you the read bits and leaves you in the Exit1-state.
JTAGpath- functions that combine multiple statecalls into one function assume you're in RunTestIdle (or Update) state before calling them.



Available console functions:
testIR:
Shift in a pattern to find out the default IR and its length, then go to Pause-IR and assert TRST.

testDR:
Similar to testIR but for the default DR.

instCheckDR binaryinstnum:
Check what is the value of DR after selecting instruction binaryinstnum. To be a little bit safer, it doesn't go to Update the DR after reading it but resets the system.
Remember to give the binaryinstnum with correct amount of bits, eg. instCheckDR 0001

instCheckIR binaryinstnum:
Like instCheckDR but checks IR instead (to see for example if it is different/different length due to moving to a different mode).

eJTAGdump startaddress endaddress:
Uses EJTAG instructions to (slowly) read the targets memory space and outputs a hex dump. Uses word-size (32-bit) reads. Assumes EJTAG mode active.
Give startaddress and endaddress as hex values. 
Read the function description in code and consider if it suits for your use before trying.



Note that JTAG is not a safe interface to experiment on if you have no idea what instructions you're running!
Hope you don't accidentally run any ROM Erase instructions... or force any pins to a state they shouldn't be at.


main.c, jtagbitbang.c, jtagbitbang.h, hwdep.c, hwdep.h (c) 2023 ntt (0BSD)
other sources from Linus Remahl(https://github.com/linusreM/Longan-RISC-V-examples), and Gigadevice (BSD)
