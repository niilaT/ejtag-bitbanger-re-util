/*
Copyright (C) 2023 by ntt

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING 
ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, 
DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, 
WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE 
OR PERFORMANCE OF THIS SOFTWARE.
*/

#ifndef HWDEP_H
#define HWDEP_H

#include "gd32vf103.h"
#include "bittype.h"



// GPIOB pins on Longan Nano
#define TRST GPIO_PIN_9
#define TDI GPIO_PIN_8
#define TDO GPIO_PIN_7
#define TMS GPIO_PIN_6
#define TCK GPIO_PIN_5
#define SYSRST GPIO_PIN_4
 #define SYSRST_ACTIVELOW 1
#define DINT GPIO_PIN_1
 #define DINT_ACTIVELOW 1

void init_jtag_pins();

void set_half_tck_timing(unsigned int us); //default 5us -> 100kHz

void wait_half_tck();
void wait_us(unsigned int us);

void set_pin_high(unsigned int pin);

void set_pin_low(unsigned int pin);

bit read_pin(unsigned int pin);

#endif /* HWDEP_H */
