/*
Copyright (C) 2023 by ntt

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING 
ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, 
DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, 
WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE 
OR PERFORMANCE OF THIS SOFTWARE.
*/

#ifndef JTAGBITBANG_H
#define JTAGBITBANG_H
#include <inttypes.h>
#include "bittype.h"


void init_jtag();
void change_half_tck_us(unsigned int us);


void assert_TRST();

void clear_TRST();

void assert_SYSRST();

void clear_SYSRST();

void triggerDINT();




void toTestLogicReset();

void toRunTestIdle();

void toSelectDRScan();

void toSelectIRScan();

void toCaptureIR();

void toCaptureDR();

void toShiftIR();

void toShiftDR();

//always call toShiftIR() before this
void readShiftIRgotoExit1IR(bit *TDIbits, bit *TDObits, unsigned int n);

//always call toShiftDR() before this
void readShiftDRgotoExit1DR(bit *TDIbits, bit *TDObits, unsigned int n);

//call this from toCaptureIR to go directly to Exit1IR
void toExit1IRskipShiftIR();

//call this from toCaptureDR to go directly to Exit1DR
void toExit1DRskipShiftDR();

void toPauseIR();

void toPauseDR();

void toExit2IR();

void toExit2DR();

void toUpdateIR();

void toUpdateDR();



//---------------combine multiple state transitions into one call

//always move to RunTestIdle or Update state before calling these
void JTAGpathIRtoUpdate(bit TDIbits[], bit TDObits[], unsigned int n);
void JTAGpathDRtoUpdate(bit TDIbits[], bit TDObits[], unsigned int n);



//---------------utils

void bitsToUint32(bit bitarray[32], uint32_t* value32);
void uint32ToBits(uint32_t value32, bit bitarray[32]);

#endif /* JTAGBITBANG_H */