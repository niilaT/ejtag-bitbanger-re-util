/*
Copyright (C) 2023 by ntt

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING 
ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, 
DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, 
WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE 
OR PERFORMANCE OF THIS SOFTWARE.
*/

#include "hwdep.h"
#include "systick.h"

unsigned int tck_halfperiod_us = 5; // 2*5us -> 100kHz

void init_jtag_pins()
{
    gpio_deinit(GPIOB);

    rcu_periph_clock_enable(RCU_GPIOB);

    //GPIO pin in normal open drain mode 0->grounded 1->high-z

    //TRST supposed to be active low (+external pull-up)
    gpio_init(GPIOB, GPIO_MODE_OUT_OD, GPIO_OSPEED_50MHZ, TRST);
    gpio_bit_reset(GPIOB, TRST); //grounded, external pull-up -> 0

    //TDI on JTAG connector
    //sampled on rising edge on TCK
    gpio_init(GPIOB, GPIO_MODE_OUT_OD, GPIO_OSPEED_50MHZ, TDI);
    gpio_bit_set(GPIOB, TDI); //hi-z, external pull-up -> 1

    //TDO on JTAG connector
    //data is changed after falling edge of TCK, should be sampled on rising edge
    gpio_init(GPIOB, GPIO_MODE_IN_FLOATING, GPIO_OSPEED_50MHZ, TDO);

    //TMS on JTAG connector
    //sampled on rising edge of TCK
    gpio_init(GPIOB, GPIO_MODE_OUT_OD, GPIO_OSPEED_50MHZ, TMS);
    gpio_bit_set(GPIOB, TMS); //hi-z, external pull-up -> 1

    //TCK on JTAG connector
    gpio_init(GPIOB, GPIO_MODE_OUT_OD, GPIO_OSPEED_50MHZ, TCK);
    gpio_bit_set(GPIOB, TCK); //hi-z, external pull-up -> 1

    //SYSRST supposed to be active low
    gpio_init(GPIOB, GPIO_MODE_OUT_OD, GPIO_OSPEED_50MHZ, SYSRST);
    gpio_bit_set(GPIOB, SYSRST); //hi-z, external pull-up -> 1

    //DINT on this system is active low
    gpio_init(GPIOB, GPIO_MODE_OUT_OD, GPIO_OSPEED_50MHZ, DINT);
    gpio_bit_set(GPIOB, DINT); //hi-z, external pull-up -> 1
}

void set_half_tck_timing(unsigned int us)
{
    tck_halfperiod_us = us;
}

void wait_half_tck()
{
    delay_1us(tck_halfperiod_us);
}

void wait_us(unsigned int us)
{
    delay_1us(us);
}

void set_pin_high(unsigned int pin)
{
    gpio_bit_set(GPIOB, pin);
}

void set_pin_low(unsigned int pin)
{
    gpio_bit_reset(GPIOB, pin);
}

bit read_pin(unsigned int pin)
{
    return gpio_input_bit_get(GPIOB, pin);
}
