/*
Copyright (C) 2023 by ntt

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING 
ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, 
DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, 
WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE 
OR PERFORMANCE OF THIS SOFTWARE.
*/

#include "jtagbitbang.h"
#include "hwdep.h"


void init_jtag()
{
    init_jtag_pins();
}

void change_half_tck_us(unsigned int us)
{
    set_half_tck_timing(us);
}


void assert_TRST()
{
    set_pin_low(TRST);
}

void clear_TRST()
{
    set_pin_high(TRST);
}

void assert_SYSRST()
{
    if (SYSRST_ACTIVELOW)
        set_pin_low(SYSRST);
    else
        set_pin_high(SYSRST);
}

void clear_SYSRST()
{
    if (SYSRST_ACTIVELOW)
        set_pin_high(SYSRST);
    else
        set_pin_low(SYSRST);
}

void triggerDINT()
{
    if (DINT_ACTIVELOW)
    {
        set_pin_low(DINT);
        wait_us(2); //according to the EJTAG spec, minimum pulse width for DINT is 1us
        set_pin_high(DINT);
    }
    else
    {
        set_pin_high(DINT);
        wait_us(2); //according to the EJTAG spec, minimum pulse width for DINT is 1us
        set_pin_low(DINT);
    }
    
}







void toTestLogicReset()
{
    set_pin_high(TMS);
    wait_half_tck();
    set_pin_high(TCK);
    wait_half_tck();
    set_pin_low(TCK);
    wait_half_tck();
    set_pin_high(TCK);
    wait_half_tck();
    set_pin_low(TCK);
    //in Test-Logic Reset state as long as TMS=1
}

void toRunTestIdle()
{
    //move to Run-Test/Idle state, TMS->0
    set_pin_low(TMS);
    wait_half_tck();
    set_pin_high(TCK);
    wait_half_tck();
    set_pin_low(TCK);
    //here as long as TMS=0
}

void toSelectDRScan()
{
    //move to Select-DR-Scan state, TMS->1
    set_pin_high(TMS);
    wait_half_tck();
    set_pin_high(TCK);
    wait_half_tck();
    set_pin_low(TCK);    
}

void toSelectIRScan()
{
    //move to Select-IR-Scan state, TMS->1
    //TMS already high from the previous state
    wait_half_tck();
    set_pin_high(TCK);
    wait_half_tck();
    set_pin_low(TCK);
}

void toCaptureIR()
{
    //move to Capture-IR state, TMS->0
    set_pin_low(TMS);
    wait_half_tck();
    set_pin_high(TCK);
    wait_half_tck();
    set_pin_low(TCK);
    //Shift-IR loaded with the default instruction after entering this state
}

void toCaptureDR()
{
    //move to Capture-DR state, TMS->0
    toCaptureIR(); //equal logic
    //Shift-DR register may contain IDCODE after entering this state if IDCODE is the default instruction after reset
}

void toShiftIR()
{
    //move to Shift-IR state, TMS->0
    set_pin_low(TMS);
    wait_half_tck();
    set_pin_high(TCK);
    wait_half_tck();
    set_pin_low(TCK); //first TDO will be set after this; first TDI should be set after this
    //here as long as TMS=0
}

void toShiftDR()
{
    //move to Shift-DR state, TMS->0
    toShiftIR(); //equal logic
    //here as long as TMS=0
}

// always call toShiftIR() before this
// bitarray is in natural reading order where MSB is on the left in array index 0 and LSB in the highest array index n-1
void readShiftIRgotoExit1IR(bit* TDIbits, bit* TDObits, unsigned int n)
{
    for (int i=(n-1); i>=1; i--)
    {
        //shift data in via TDI and read the loaded instruction via TDO
        if (TDIbits[i]==1)
        {
            set_pin_high(TDI); //1
        }
        else
        {
            set_pin_low(TDI); //0
        }
        
        wait_half_tck();
        set_pin_high(TCK);
        TDObits[i] = read_pin(TDO); //read TDO
        wait_half_tck();
        set_pin_low(TCK); //TDO will be set after this
    }

    if (TDIbits[0]==1)
    {
        set_pin_high(TDI); //1
    }
    else
    {
        set_pin_low(TDI); //0
    }
    //move to Exit1-IR state, TMS->1
    set_pin_high(TMS);
    wait_half_tck();
    set_pin_high(TCK);
    TDObits[0] = read_pin(TDO); //read last TDO
    wait_half_tck();
    set_pin_low(TCK);
}

//always call toShiftDR() before this
void readShiftDRgotoExit1DR(bit* TDIbits, bit* TDObits, unsigned int n)
{
    readShiftIRgotoExit1IR(TDIbits, TDObits, n); //equal logic
}

//call this from toCaptureIR to go directly to Exit1IR
void toExit1IRskipShiftIR()
{
    //move to Exit1-IR state, TMS->1
    set_pin_high(TMS);
    wait_half_tck();
    set_pin_high(TCK);
    wait_half_tck();
    set_pin_low(TCK);
}

//call this from toCaptureDR to go directly to Exit1DR
void toExit1DRskipShiftDR()
{
    //move to Exit1-DR state, TMS->1
    toExit1IRskipShiftIR(); //equal logic
}

void toPauseIR()
{
    //move to Pause-IR state, TMS->0
    set_pin_low(TMS);
    wait_half_tck();
    set_pin_high(TCK);
    wait_half_tck();
    set_pin_low(TCK);
    //here as long as TMS=0
}

void toPauseDR()
{
    //move to Pause-DR state, TMS->0
    toPauseIR(); //equal logic
    //here as long as TMS=0
}

void toExit2IR()
{
    //move to Exit2-IR state, TMS->1
    set_pin_high(TMS);
    wait_half_tck();
    set_pin_high(TCK);
    wait_half_tck();
    set_pin_low(TCK);
}

void toExit2DR()
{
    //move to Exit2-DR state, TMS->1
    toExit2IR(); //equal logic
}

void toUpdateIR()
{
    //move to Update-IR state, TMS->1
    set_pin_high(TMS);
    wait_half_tck();
    set_pin_high(TCK);
    wait_half_tck();
    set_pin_low(TCK);
}

void toUpdateDR()
{
    //move to Update-DR state, TMS->1
    toUpdateIR(); //equal logic
}




//---------------combine multiple state transitions into one call

//always move to RunTestIdle or Update state before calling
void JTAGpathIRtoUpdate(bit TDIbits[], bit TDObits[], unsigned int n)
{
    toSelectDRScan();
    toSelectIRScan();
    toCaptureIR();
    toShiftIR();
    readShiftIRgotoExit1IR(TDIbits, TDObits, n);
    toUpdateIR();
}

//always move to RunTestIdle or Update state before calling
void JTAGpathDRtoUpdate(bit TDIbits[], bit TDObits[], unsigned int n)
{
    toSelectDRScan();
    toCaptureDR();
    toShiftDR();
    readShiftDRgotoExit1DR(TDIbits, TDObits, n);
    toUpdateDR();
}




//-----------------utils

void bitsToUint32(bit bitarray[32], uint32_t* value32)
{
    for (int i = 0; i < 32; ++i) {
        *value32 = *value32 << 1;
        *value32 = bitarray[i] ? *value32 | 1U : *value32 & 0xFFFFFFFE;
    }
}

void uint32ToBits(uint32_t value32, bit bitarray[32])
{
    unsigned int mask = 1U << 31;
    for (int i = 0; i < 32; ++i) {
        bitarray[i] = (value32 & mask) ? 1 : 0;
        value32 = value32 << 1;
    }
}
