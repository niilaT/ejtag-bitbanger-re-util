// eJTAG bitbanger - RE util
/*
Copyright (C) 2023 by ntt

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING 
ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, 
DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, 
WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE 
OR PERFORMANCE OF THIS SOFTWARE.
*/

#include "gd32vf103.h"
#include <stdio.h>
#include <string.h>
#include "usb_serial_if.h"
#include "jtagbitbang.h"

void testIR();
void testDR();
void eJTAGdump(uint32_t addr1, uint32_t addr2);
void instCheckDR(bit IRinput[], unsigned int instLen);
void instCheckIR(bit IRinput[], unsigned int instLen);


char received_command[64] = {'\0'};


int main(void)
{

    configure_usb_serial();

    init_jtag();

    eclic_global_interrupt_enable();
    eclic_priority_group_set(ECLIC_PRIGROUP_LEVEL2_PRIO2);


    while(1){

        if(usb_serial_available()){

            char received_USB_characters[64] = {'\0'};           
            
            int count = read_usb_serial((uint8_t*)received_USB_characters);

            if(count > 0){

                strncat(received_command, received_USB_characters, (63-strlen(received_command)));
                if (strlen(received_command)==63) received_command[0] = '\0';

                // received a newline from the USB serial?
                if( strpbrk(received_command, "\n") )
                {
                    // truncate the command string to remove any newline characters
                    received_command[strcspn(received_command, "\r\n")] = '\0';

                    
                    if( strcmp(received_command, "testIR")==0 ) // testIR
                    {
                        printf("%s\n", "testIR");
                        testIR();
                        printf("%s\n", "done.");
                    }
                    else if( strcmp(received_command, "testDR")==0 ) // testDR
                    {
                        printf("%s\n", "testDR");
                        testDR();
                        printf("%s\n", "done.");
                    }
                    else if( strncmp(received_command, "instCheckDR ", 12)==0 ) // instCheckDR 1111
                    {
                    
                        unsigned int binInputLen = strlen(received_command) - 12;
                        if (binInputLen > 0)
                        {
                            bool binaryInputOK = true;
                            bit IRinput[binInputLen];
                            for (int i=0;i<binInputLen;++i)
                            {
                                uint8_t inputNum = received_command[i+12]-'0';
                                if (inputNum != 0 && inputNum != 1)
                                {
                                    binaryInputOK = false;
                                    break;
                                }
                                IRinput[i] = inputNum;

                            }

                            if (binaryInputOK)
                            {
                                printf("%s\n", "instCheckDR");
                                instCheckDR(IRinput, binInputLen);
                                printf("%s\n", "done.");
                            }
                            else
                            {
                                printf("%s\n", "Wrong input: give IR as binary bits");
                            }
                        }
                        else
                        {
                            printf("%s\n", "Wrong input: function call format instCheckDR 1111");
                        }
                        
                    }
                    else if( strncmp(received_command, "instCheckIR ", 12)==0 ) // instCheckIR 1111
                    {
                    
                        unsigned int binInputLen = strlen(received_command) - 12;
                        if (binInputLen > 0)
                        {
                            bool binaryInputOK = true;
                            bit IRinput[binInputLen];
                            for (int i=0;i<binInputLen;++i)
                            {
                                uint8_t inputNum = received_command[i+12]-'0';
                                if (inputNum != 0 && inputNum != 1)
                                {
                                    binaryInputOK = false;
                                    break;
                                }
                                IRinput[i] = inputNum;

                            }

                            if (binaryInputOK)
                            {
                                printf("%s\n", "instCheckIR");
                                instCheckDR(IRinput, binInputLen);
                                printf("%s\n", "done.");
                            }
                            else
                            {
                                printf("%s\n", "Wrong input: give IR as binary bits");
                            }
                        }
                        else
                        {
                            printf("%s\n", "Wrong input: function call format instCheckIR 1111");
                        }
                        
                    }
                    else if( strncmp(received_command, "eJTAGdump ", 10)==0 ) // eJTAGdump 00000000 ffffffff
                    {
                        int conversion_err = 0;
                        char* firstnum_endptr;
                        uint32_t addr1 = 0;
                        if (received_command[10]!=0)
                        {
                            addr1 = strtoul(&received_command[10],&firstnum_endptr,16);
                        }
                        else conversion_err = 1;

                        uint32_t addr2 = 0;
                        if ( *firstnum_endptr!=0 && *(firstnum_endptr+1)!=0)
                        {
                            addr2 = strtoul(firstnum_endptr+1,NULL,16);
                        }
                        else conversion_err = 2;
                       
                        if ( addr1 > addr2 ) conversion_err = 3;

                        if ( conversion_err==0 && (addr1 & 0x03) != 0)
                        {
                            printf("%s\n", "Auto-aligning start address to word boundary...");
                            addr1 >>= 2; addr1 <<= 2;
                        }
                        
                        if ( conversion_err==0 )
                        {
                            printf("%s %#010x %#010x\n", "eJTAGdump", (unsigned int)addr1, (unsigned int)addr2);
                            eJTAGdump(addr1, addr2);
                            printf("%s\n", "done.");
                        }
                        else printf("%s\n", "Wrong input: give start and end hex values eg. eJTAGdump 00000000 ffffffff");
                    }
                    
                    else
                    {
                        printf("%s\n", "Wrong command");
                    }

                    
                    received_command[0] = '\0';
                }
                
            }
            
        }
    }


    while(1);
}





void testIR()
{
    clear_TRST();
    
    toTestLogicReset();
    toRunTestIdle();
    toSelectDRScan();
    toSelectIRScan();
    toCaptureIR();
    toShiftIR();
    bit bitsIn[16] = {1,1,1,1,0,0,0,1,1,1,0,0,1,1,0,1};
    bit bitsOut[16];
    readShiftIRgotoExit1IR(bitsIn, bitsOut, 16);
    toPauseIR();

    assert_TRST();
    
    printf("%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",bitsOut[0],bitsOut[1],bitsOut[2],bitsOut[3],bitsOut[4],bitsOut[5],bitsOut[6],bitsOut[7],bitsOut[8],bitsOut[9],bitsOut[10],bitsOut[11],bitsOut[12],bitsOut[13],bitsOut[14],bitsOut[15]);

}

void testDR()
{
    clear_TRST();
    
    toTestLogicReset();
    toRunTestIdle();
    toSelectDRScan();
    toCaptureDR();
    toShiftDR();
    //shift through default DR eg. 32x4 times. Default DR may be eg. 1-bit BYPASS or IDCODE
    bit bitsIn[32*4] = {0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,1,1,1,1,1,0,0,0,0,1,1,1,1,0,0,0,1,1,1,0,0,1,1,0,1};
    bit bitsOut[32*4];
    readShiftDRgotoExit1DR(bitsIn, bitsOut, 32*4);
    toPauseDR();

    assert_TRST();

    //print received TDO bits
    char string[32*4*2+1] = {'\0'};
    char substring[3] = {'\0'};
    for(int i=0; i<32*4; ++i)
    {
        sprintf(substring,"%1u,",bitsOut[i]);
        strcat(string, substring);
    }
    printf("%s\n", string);
}

// NOTE1: Assumes external EJTAG Enable signal asserted or otherwise in EJTAG mode by default - do not run if you're not sure!
// NOTE2: Assumes just a single Mips16e/Mips32 device in the JTAG chain.
// NOTE3: This maintains the MIPS32 dump code in JTAG probe's dmseg memory area which is supposed to be uncacheable and not real memory, so any fetches into it will always have to be handled by the probe (this code).
//        With all the overhead from JTAG protocol, transfer speed is very low.
//        Much faster speeds could be achieved by saving the dumper into the target MCU's RAM and using FASTDATA to read the output, but that would require making assumptions about/knowing the target's RAM address space.
//        The code (see below) could also be slightly optimized by loop unwrapping, allowing multiple reading runs before having to input the branch ASM to jump back to beginning, but gains from that would be small.
// NOTE4: If processor access to dmseg was not working because of host firmware code somehow interfering then could try EJTAGBOOT instruction to reset and jump directly into dmseg, or triggering DINT or EjtagBrk very early in boot
void eJTAGdump(uint32_t addr1, uint32_t addr2)
{
    //#define DEBUG_EJTAG

    if ( (addr1<0xFF300000U) && (addr2>=0xFF200000U) )
    {
        printf("%s\n", "Dump address range includes dmseg, aborting. Don't try to dump dmseg.");
        return;
    }

    //clear_SYSRST();
    clear_TRST();

    toTestLogicReset();
    toRunTestIdle();

    // select CONTROL instruction
    bit instCONTROL[8] = {0,0,0,0,1,0,1,0};
    bit out_i[8];
    JTAGpathIRtoUpdate(instCONTROL, out_i, 8);
    
    // update CONTROL data register
    // first clear Rocc bit in Control register to acknowledge that processor had reset; Control register won't be updated in Update-DR unless cleared
    bit regCONTROL[32] = {0}; //write zero to clear bit31 Rocc
    regCONTROL[(32-1)-18] = 1; //PrAcc should be written 1 to ignore - assuming there isn't a pending request yet
    regCONTROL[(32-1)-15] = 1; //ProbEn; JTAG probe will serve processor accesses
    regCONTROL[(32-1)-14] = 1; //ProbTrap; debug exception vector is located in the probe dmseg memory area 0xFF20 0200
    bit out_d[32];
    JTAGpathDRtoUpdate(regCONTROL, out_d, 32);

    // trigger debug exception in CONTROL register to start the process of executing our own code on the CPU
    regCONTROL[(32-1)-31] = 1; //write 1 to Rocc after first ack; will safely stall our further actions if reset occurs in middle of everything
    regCONTROL[(32-1)-12] = 1; //we can use EjtagBrk instead of separate DINT to trigger a read to the dmseg area debug exception handler that we will provide
    JTAGpathDRtoUpdate(regCONTROL, out_d, 32);
    regCONTROL[(32-1)-12] = 0; // no need to trigger EjtagBrk again

            #ifdef DEBUG_EJTAG
            printf("%s\n", "[EjtagBrk triggered]");
            #endif

    // according to the ejtag standard ISA mode is automatically set to 32-bit when entering debug mode on a MIPS16e CPU
    // CODE MAINTAINED FOR DMSEG:
    //   FF200200: lui $t0, (addr1 >> 16)          : set $t0 to start address (hi)
    //   FF200204: ori $t0, $t0, (addr1 & 0x0FFFF) : set $t0 to start address (low)
    //   FF200208: lui $t1, 0xFF20                 : set $t1 to destination address
    // />FF20020C: lw $t2, 0($t0)                  : load value from *$t0 to $t2
    // | FF200210: sw $t2, 0($t1)                  : store to $t1 (FF200000) (FASTDATA-able area is FF200000-0F although we're not using it now)
    // | FF200214: b FF20020C                      : branch back to FF20020C
    // |           [OUTPUT from the previous sw instruction] (output seems to actually occur here, after the branch instruction fetch)
    // \-FF200218:   addiu $t0, $t0, 4             : $t0 += 4 (in delay slot)

    uint32_t ASMprogram[7] = {(0x3C080000|(addr1>>16)),(0x35080000|(addr1&0x0FFFF)),0x3C09FF20,0x8D0A0000,0xAD2A0000,0x1000FFFD,0x25080004}; //MIPS32 Big endian
    unsigned int ASMprogramLoopStart = 0;
    unsigned int programTotalSteps = 8;
    
    unsigned int linePos = 0;
    for (uint64_t caddr = addr1; caddr<addr2; caddr+=4) //every program cycle 32 bits are read
    {
        uint32_t data = 0;
        for (unsigned int programStep=ASMprogramLoopStart, ASMindex=ASMprogramLoopStart; programStep<programTotalSteps; ++programStep)
        {

                    #ifdef DEBUG_EJTAG
                    printf("\n[Waiting for PrAcc]");
                    #endif

            // Wait for PrAcc to become 1 signifying there is a pending access to the dmseg area that we should handle.
            // PrAcc may not synchronize immediately and pending access size and ADDRESS register might get updated before it.
            // We could probably just ignore reading it as long as the JTAG speed is slow enough, but let's handle it properly anyways.
            while(1) //TODO could probably add a timeout and error-reset for this...
            {
                JTAGpathDRtoUpdate(regCONTROL, out_d, 32);
                if ( out_d[(32-1)-18] == 1 ) //PrAcc == 1 ?
                {
                    break;
                }
            }

                    #ifdef DEBUG_EJTAG
                    bit instADDRESS[8] = {0,0,0,0,1,0,0,0};
                    JTAGpathIRtoUpdate(instADDRESS, out_i, 8);
                    bit dummyAddr[32] = {0};
                    bit regADDRESS[32] = {0};
                    JTAGpathDRtoUpdate(dummyAddr, regADDRESS, 32);
                    uint32_t debugAddress = 0;
                    bitsToUint32(regADDRESS, &debugAddress);
                    printf("[PrAcc: Pending %s of size %s for ADDRESS 0x%08X]", (out_d[(32-1)-19] ? "WRITE" : "READ"), (out_d[(32-1)-29] ? (out_d[(32-1)-30] ? "TRIPLE" : "HALFWORD") : (out_d[(32-1)-30] ? "WORD" : "BYTE")), (unsigned int)debugAddress);
                    //JTAGpathIRtoUpdate(instCONTROL, out_i, 8);
                    #endif

            // output asm instruction to the DATA register, or read it if we're in the read cycle
            bit instDATA[8] = {0,0,0,0,1,0,0,1};
            JTAGpathIRtoUpdate(instDATA, out_i, 8);
            if (out_d[(32-1)-19] == 0) // PRnW CPU read pending - we need to write
            {
                uint32_t asmUint = ASMprogram[ASMindex];
                        #ifdef DEBUG_EJTAG
                        printf("[Output ASM 0x%08X to DATA]", (unsigned int)asmUint);
                        #endif
                bit asmBits[32] = {0};
                uint32ToBits(asmUint, asmBits);
                JTAGpathDRtoUpdate(asmBits, out_d, 32);
                ++ASMindex;
            }
            else
            {
                // this cycle we read the output data instead of writing an ASM instruction
                bit dummyData[32] = {0};
                bit dataRead[32] = {0};
                JTAGpathDRtoUpdate(dummyData, dataRead, 32);
                bitsToUint32(dataRead, &data);
                        #ifdef DEBUG_EJTAG
                        printf("[Read DATA: 0x%08x]", (unsigned int)data);
                        #endif
            }

            // acknowledge PrAcc
            JTAGpathIRtoUpdate(instCONTROL, out_i, 8);
            regCONTROL[(32-1)-18] = 0;
            JTAGpathDRtoUpdate(regCONTROL, out_d, 32);
            regCONTROL[(32-1)-18] = 1;

            ASMprogramLoopStart = 3;
        }

        // print the four bytes of data we got
        unsigned char u0 = (data & 0xFF000000) >> 24, u1 = (data & 0x00FF0000) >> 16, u2 = (data & 0x0000FF00) >> 8, u3 = (data & 0x0FF);
        printf("%02x %02x %02x %02x  ", u0, u1, u2, u3);
        linePos += 4;
        if (linePos >= 16)
        {
            linePos = 0;
            printf("\n");
        }
    }

    // just leave the target halted after dump and then reset it; could also send it a DERET ASM instruction (0x4200001f) to return to normal operation if it matters
    assert_TRST();

    assert_SYSRST();
}

void instCheckDR(bit IRinput[], unsigned int instLen)
{
    char IRstring[instLen+1]; IRstring[0] = '\0';
    char IRsubstring[2] = {'\0'};
    for(int i=0; i<instLen; ++i)
    {
        sprintf(IRsubstring,"%1u",IRinput[i]);
        strcat(IRstring, IRsubstring);
    }
    printf("Testing DR after instruction %s and then Resetting system...\n", IRstring);

    //clear_SYSRST();
    clear_TRST();

    toTestLogicReset();
    toRunTestIdle();
    //triggerDINT();
    toSelectDRScan();
    toSelectIRScan();
    toCaptureIR();
    toShiftIR();
    bit bitsOut[instLen];
    readShiftIRgotoExit1IR(IRinput, bitsOut, instLen);
    toUpdateIR();
    
    toSelectDRScan();
    toCaptureDR();
    toShiftDR();
    // 1 *may be* safer output value. Not so good for finding out length of DR so you might want to change this like in testDR().
    bit DRbitsIn[32*4]; for (int i=0; i<32*4; ++i) DRbitsIn[i] = 1;
    bit DRbitsOut[32*4];
    readShiftDRgotoExit1DR(DRbitsIn, DRbitsOut, 32*4);

    //assert JTAG RESET
    assert_TRST();

    //assert SYSTEM RESET
    assert_SYSRST();

    //print received TDO bits
    char string[32*4*2+1] = {'\0'};
    char substring[3] = {'\0'};
    for(int i=0; i<32*4; ++i)
    {
        sprintf(substring,"%1u,",DRbitsOut[i]);
        strcat(string, substring);
    }
    printf("DR: %s\n", string);

}

void instCheckIR(bit IRinput[], unsigned int instLen)
{
    char IRstring[instLen+1]; IRstring[0] = '\0';
    char IRsubstring[2] = {'\0'};
    for(int i=0; i<instLen; ++i)
    {
        sprintf(IRsubstring,"%1u",IRinput[i]);
        strcat(IRstring, IRsubstring);
    }
    printf("Testing IR after instruction %s and then Resetting system...\n", IRstring);

    //clear_SYSRST();
    clear_TRST();

    toTestLogicReset();
    toRunTestIdle();
    toSelectDRScan();
    toSelectIRScan();
    toCaptureIR();
    toShiftIR();
    bit bitsOut[instLen];
    readShiftIRgotoExit1IR(IRinput, bitsOut, instLen);
    toUpdateIR();
    
    toSelectDRScan();
    toSelectIRScan();
    toCaptureIR();
    toShiftIR();
    bit IRbitsIn[16] = {1,1,1,1,0,0,0,1,1,1,0,0,1,1,0,1};
    bit IRbitsOut[16];
    readShiftIRgotoExit1IR(IRbitsIn, IRbitsOut, 16);

    //assert JTAG RESET
    assert_TRST();

    //assert SYSTEM RESET
    assert_SYSRST();

    //print received TDO bits
    char string[16*2+1] = {'\0'};
    char substring[3] = {'\0'};
    for(int i=0; i<16; ++i)
    {
        sprintf(substring,"%1u,",IRbitsOut[i]);
        strcat(string, substring);
    }
    printf("IR: %s\n", string);

}
